import java.util.Scanner;
import java.util.TreeSet;

public class MinimumLoss {
    public static void main(String[] args) {
        TreeSet<Long> prices = new TreeSet<>();
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        long bestValue = Long.MAX_VALUE;

        for(long i = 0; i < n; i++) {
            long price = in.nextLong();
            prices.add(price);
            Long closest = prices.higher(price);
            if(closest != null) {
                bestValue = Math.min(bestValue, closest - price);
            }
        }
        System.out.println(bestValue);
        in.close();
    }
}
