import java.util.HashMap;
import java.util.Scanner;

class TrieNode {
    boolean endOfWord;
    HashMap<Character, TrieNode> map;
    HashMap<Character, Integer> charCounter;

    public TrieNode() {
        map = new HashMap<>();
        charCounter = new HashMap<>();
        endOfWord = false;
    }
}

class Trie {
    TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode current = root;
        for(int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            TrieNode node = current.map.get(ch);
            if(node == null) {
                node = new TrieNode();
                current.map.put(ch, node);
                current.charCounter.put(ch, 1);
            } else {
                current.charCounter.put(ch, current.charCounter.get(ch) + 1);
            }
            current = node;
        }
        current.endOfWord = true;
    }

    public int search(String word) {
        TrieNode current = root;
        for(int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            TrieNode node = current.map.get(ch);

            if(node == null) {
                return 0;
            }

            if(i != word.length() - 1) {
                current = node;
            }
        }

        Integer i = current.charCounter.get(word.charAt(word.length() - 1));
        if(i == null) {
            return 0;
        }

        return i;
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Trie trie = new Trie();

        for(int i = 0; i < n; i++) {
            String op = in.next();
            String contact = in.next();

            if(op.equals("add")) {
                trie.insert(contact);
            } else if(op.equals("find")) {
                System.out.println(trie.search(contact));
            }
        }
    }
}