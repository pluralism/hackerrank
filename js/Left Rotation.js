function rotLeft(a, d) {
    let aCopy = a.slice();

    for(let i = 0; i < a.length; i++) {
        if(i - d < 0) {
            aCopy[a.length + (i - d)] = a[i];
        } else {
            aCopy[(i - d) % a.length] = a[i];
        }
    }

    return aCopy;
}