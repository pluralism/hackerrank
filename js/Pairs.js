function pairs(target, arr) {
    let count = 0;
    let numbersSet = new Set(arr);
    numbersSet.forEach(value => {
        if(numbersSet.has(value - target)) {
            count++;
        }
    });
    return count;
}