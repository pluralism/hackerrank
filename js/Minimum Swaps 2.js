function minimumSwaps(arr) {
    let swaps = 0;

    const swapArrayElements = (a, x, y) => {
        if(a.length === 1) {
            return;
        }
        a.splice(y, 1, a.splice(x, 1, a[y])[0]);
    };

    for(let i = 0; i < arr.length; i++) {
        if(arr[i] == i + 1) {
            continue;
        }
        swapArrayElements(arr, i, arr[i] - 1);
        i--;
        swaps++;
    }

    return swaps;
}