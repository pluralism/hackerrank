function substrCount(n, s) {
    let count = 0;

    for(let i = 0; i < n; i++) {
        let midIndex = i + 1;

        while(midIndex < n && s[midIndex] == s[i]) {
            midIndex++;
            count++;
        }

        let j = midIndex + 1;
        if(j < n) {
            while(j < n && j - midIndex <= midIndex - i && s[j] == s[i]) { 
                j++; 
            }

            if(j - midIndex - 1 == midIndex - i) {
                count++;
            }
        }
    }

    return n + count;
}