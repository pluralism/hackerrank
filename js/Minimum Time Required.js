function minTime(machines, goal) {
    machines.sort();
    let start = machines[0];
    let end = goal * machines[machines.length - 1];

    while(start < end) {
        let mid = Math.floor((start + end) / 2);
        let items = machines.reduce((acc, value) => acc + Math.floor(mid / value), 0);

        if(items < goal) {
            start = mid + 1;
        } else {
            end = mid;
        }
    }
    return start;
}