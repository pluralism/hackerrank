function checkMagazine(magazine, note) {
    var m = new Map();
    for(let word of magazine) {
        if(!m.has(word)) {
            m.set(word, 1);
        } else {
            m.set(word, m.get(word) + 1);
        }
    }

    for(let word of note) {
        if(!m.has(word) || m.get(word) == 0) {
            console.log('No');
            return;
        }
        m.set(word, m.get(word) - 1);
    }

    console.log('Yes');
}