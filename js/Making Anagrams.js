function makeAnagram(a, b) {
    let aMap = new Map();
    let bMap = new Map();
    let count = 0;

    let mapBuilder = (s, map) => {
        for(let ch of s) {
            if(map.has(ch)) {
                map.set(ch, map.get(ch) + 1);
            } else {
                map.set(ch, 1);
            }
        }
    };

    mapBuilder(a, aMap);
    mapBuilder(b, bMap);

    aMap.forEach((v, k) => {
        if(bMap.has(k)) {
            count += Math.abs(v - bMap.get(k));
        } else {
            count += aMap.get(k);
        }
    });

    bMap.forEach((v, k) => {
        if(!aMap.has(k)) {
            count += v;
        }
    });

    return count;
}